// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "MyProject.generated.dep.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode1MyProject() {}
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	ENGINE_API class UClass* Z_Construct_UClass_UActorComponent();
	COREUOBJECT_API class UScriptStruct* Z_Construct_UScriptStruct_FColor();
	COREUOBJECT_API class UScriptStruct* Z_Construct_UScriptStruct_FVector();

	MYPROJECT_API class UClass* Z_Construct_UClass_UGravityBody_NoRegister();
	MYPROJECT_API class UClass* Z_Construct_UClass_UGravityBody();
	MYPROJECT_API class UClass* Z_Construct_UClass_UGravityComponent_NoRegister();
	MYPROJECT_API class UClass* Z_Construct_UClass_UGravityComponent();
	MYPROJECT_API class UPackage* Z_Construct_UPackage__Script_MyProject();
	void UGravityBody::StaticRegisterNativesUGravityBody()
	{
	}
	UClass* Z_Construct_UClass_UGravityBody_NoRegister()
	{
		return UGravityBody::StaticClass();
	}
	UClass* Z_Construct_UClass_UGravityBody()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UActorComponent();
			Z_Construct_UPackage__Script_MyProject();
			OuterClass = UGravityBody::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20B00080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_color = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("color"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(color, UGravityBody), 0x0010000000000001, Z_Construct_UScriptStruct_FColor());
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(drawOrbit, UGravityBody, bool);
				UProperty* NewProp_drawOrbit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("drawOrbit"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(drawOrbit, UGravityBody), 0x0010000000000001, CPP_BOOL_PROPERTY_BITMASK(drawOrbit, UGravityBody), sizeof(bool), true);
				UProperty* NewProp_orbitScale = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("orbitScale"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(orbitScale, UGravityBody), 0x0010000000000001);
				UProperty* NewProp_timeScale = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("timeScale"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(timeScale, UGravityBody), 0x0010000000000001);
				UProperty* NewProp_velocity = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("velocity"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(velocity, UGravityBody), 0x0010000000000001, Z_Construct_UScriptStruct_FVector());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<UGravityBody> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("Custom"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("GravityBody.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("GravityBody.h"));
				MetaData->SetValue(NewProp_color, TEXT("Category"), TEXT("GravityBody"));
				MetaData->SetValue(NewProp_color, TEXT("ModuleRelativePath"), TEXT("GravityBody.h"));
				MetaData->SetValue(NewProp_drawOrbit, TEXT("Category"), TEXT("GravityBody"));
				MetaData->SetValue(NewProp_drawOrbit, TEXT("ModuleRelativePath"), TEXT("GravityBody.h"));
				MetaData->SetValue(NewProp_orbitScale, TEXT("Category"), TEXT("GravityBody"));
				MetaData->SetValue(NewProp_orbitScale, TEXT("ModuleRelativePath"), TEXT("GravityBody.h"));
				MetaData->SetValue(NewProp_timeScale, TEXT("Category"), TEXT("GravityBody"));
				MetaData->SetValue(NewProp_timeScale, TEXT("ModuleRelativePath"), TEXT("GravityBody.h"));
				MetaData->SetValue(NewProp_velocity, TEXT("Category"), TEXT("GravityBody"));
				MetaData->SetValue(NewProp_velocity, TEXT("ModuleRelativePath"), TEXT("GravityBody.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGravityBody, 2962227408);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGravityBody(Z_Construct_UClass_UGravityBody, &UGravityBody::StaticClass, TEXT("/Script/MyProject"), TEXT("UGravityBody"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGravityBody);
	void UGravityComponent::StaticRegisterNativesUGravityComponent()
	{
	}
	UClass* Z_Construct_UClass_UGravityComponent_NoRegister()
	{
		return UGravityComponent::StaticClass();
	}
	UClass* Z_Construct_UClass_UGravityComponent()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UActorComponent();
			Z_Construct_UPackage__Script_MyProject();
			OuterClass = UGravityComponent::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20A00080;


				static TCppClassTypeInfo<TCppClassTypeTraits<UGravityComponent> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("Custom"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("GravityComponent.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("GravityComponent.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGravityComponent, 1333023859);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGravityComponent(Z_Construct_UClass_UGravityComponent, &UGravityComponent::StaticClass, TEXT("/Script/MyProject"), TEXT("UGravityComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGravityComponent);
	UPackage* Z_Construct_UPackage__Script_MyProject()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), nullptr, FName(TEXT("/Script/MyProject")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000000);
			FGuid Guid;
			Guid.A = 0x572538D3;
			Guid.B = 0xA9DAE142;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
