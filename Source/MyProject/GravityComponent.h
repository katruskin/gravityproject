// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "orbit.h"
#include "GravityBody.h"
#include "Components/ActorComponent.h"
#include "GravityComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UGravityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGravityComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void PostLoad() override;

private:
	double time;
	TMap<UGravityBody*, EllipticalOrbit*> orbitDic;
	FVector RecalculateCoordinates(FVector input, FVector location);
	FVector RotateCoordinates(FVector input, FRotator rotation);
	void ProcessOrbits(float scale);
	void DrawOrbits();
	void Move(float delateTime);

};
