#include "stdafx.h"
#include "astro.h"
#include "orbit.h"
#include <geomutil.h>
#include <cassert>
#include <Eigen/Core>
#include <Eigen/Geometry>


class OrbitCreator
{
public:
	OrbitCreator();
	~OrbitCreator();
	EllipticalOrbit*
		CreateEllipticalOrbit(double semiMajorAxis, double period, double inclination, double eccentricity, double ascendingNode, double argOfPericenter, double anomalyAtEpoch, double epoch);

	EllipticalOrbit*
		StateVectorToOrbit(Eigen::Vector3d,  Eigen::Vector3d, double, double);

};

