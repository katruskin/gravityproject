// parser.h
//
// Copyright (C) 2001-2009, the Celestia Development Team
// Original version by Chris Laurel <claurel@gmail.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

#ifndef _PARSER_H_
#define _PARSER_H_

#include <vector>
#include <map>
#include <vecmath.h>
#include <quaternion.h>
////#include <celutil/color.h>
////#include <celutil/basictypes.h>
////#include <celengine/tokenizer.h>
#include <Eigen/Core>
#include <Eigen/Geometry>



#endif // _PARSER_H_
