// Fill out your copyright notice in the Description page of Project Settings.

#include "GravityBody.h"


// Sets default values for this component's properties
UGravityBody::UGravityBody()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	orbitScale = 1000;
	timeScale = 1;
	drawOrbit = true;
	// ...
}


// Called when the game starts
void UGravityBody::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UGravityBody::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
#if WITH_EDITOR
void UGravityBody::PostEditChangeProperty(FPropertyChangedEvent& e)
{

}

#endif
