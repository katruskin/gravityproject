// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GravityBody.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECT_API UGravityBody : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGravityBody();

	UPROPERTY(EditAnywhere)
		FVector velocity;
	UPROPERTY(EditAnywhere)
		int32 timeScale;
	UPROPERTY(EditAnywhere)
		int32 orbitScale;
	UPROPERTY(EditAnywhere)
		bool drawOrbit;
	UPROPERTY(EditAnywhere)
		FRotator  rotation;
	UPROPERTY(EditAnywhere)
		FColor color;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent);
	
	
#endif
		
	
};
