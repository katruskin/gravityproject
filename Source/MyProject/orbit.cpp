#pragma warning(disable : 4503)
#include "orbit.h"
#include "stdafx.h"
#include "parser.h"
#include "astro.h"
#include <mathlib.h>
#include <solve.h>
#include <geomutil.h>
#include <functional>
#include <algorithm>
#include <cmath>
#include <cassert>

using namespace Eigen;
using namespace std;

// Orbital velocity is computed by differentiation for orbits that don't
// override velocityAtTime().
static const double ORBITAL_VELOCITY_DIFF_DELTA = 1.0 / 1440.0;


static Vector3d cubicInterpolate(const Vector3d& p0, const Vector3d& v0,
	const Vector3d& p1, const Vector3d& v1,
	double t)
{
	return p0 + (((2.0 * (p0 - p1) + v1 + v0) * (t * t * t)) +
		((3.0 * (p1 - p0) - 2.0 * v0 - v1) * (t * t)) +
		(v0 * t));
}

/** Sample the orbit over the time range [ startTime, endTime ] using the
* default sampling parameters for the orbit type.
*
* Subclasses of orbit should override this method as necessary. The default
* implementation uses an adaptive sampling scheme with the following defaults:
*    tolerance: 1 km
*    start step: T / 1e5
*    min step: T / 1e7
*    max step: T / 100
*
* Where T is either the mean orbital period for periodic orbits or the valid
* time span for aperiodic trajectories.
*/
void Orbit::sample(double startTime, double endTime, OrbitSampleProc& proc) const
{
	double span = 0.0;
	if (isPeriodic())
	{
		span = getPeriod();
	}
	else
	{
		double startValidInterval = 0.0;
		double endValidInterval = 0.0;
		getValidRange(startValidInterval, endValidInterval);
		if (startValidInterval == endValidInterval)
		{
			span = endValidInterval - startValidInterval;
		}
		else
		{
			span = endTime - startTime;
		}
	}

	AdaptiveSamplingParameters samplingParams;
	samplingParams.tolerance = 1.0; // kilometers
	samplingParams.maxStep = span / 100.0;
	samplingParams.minStep = span / 1.0e7;
	samplingParams.startStep = span / 1.0e5;

	adaptiveSample(startTime, endTime, proc, samplingParams);

}

/** Adaptively sample the orbit over the range [ startTime, endTime ].
*/
void Orbit::adaptiveSample(double startTime, double endTime, OrbitSampleProc& proc, const AdaptiveSamplingParameters& samplingParams) const
{
	double startStepSize = samplingParams.startStep;
	double maxStepSize = samplingParams.maxStep;
	double minStepSize = samplingParams.minStep;
	double tolerance = samplingParams.tolerance;
	double t = startTime;
	const double stepFactor = 1.25;

	Vector3d lastP = positionAtTime(t);
	Vector3d lastV = velocityAtTime(t);
	proc.sample(t, lastP, lastV);
	int sampCount = 0;
	int nTests = 0;

	while (t < endTime)
	{
		// Make sure that we don't go past the end of the sample interval
		maxStepSize = min(maxStepSize, endTime - t);
		double dt = min(maxStepSize, startStepSize * 2.0);

		Vector3d p1 = positionAtTime(t + dt);
		Vector3d v1 = velocityAtTime(t + dt);

		double tmid = t + dt / 2.0;
		Vector3d pTest = positionAtTime(tmid);
		Vector3d pInterp = cubicInterpolate(lastP, lastV * dt,
			p1, v1 * dt,
			0.5);
		nTests++;

		double positionError = (pInterp - pTest).norm();

		// Error is greater than tolerance; decrease the step until the
		// error is within the tolerance.
		if (positionError > tolerance)
		{
			while (positionError > tolerance && dt > minStepSize)
			{
				dt /= stepFactor;

				p1 = positionAtTime(t + dt);
				v1 = velocityAtTime(t + dt);

				tmid = t + dt / 2.0;
				pTest = positionAtTime(tmid);
				pInterp = cubicInterpolate(lastP, lastV * dt,
					p1, v1 * dt,
					0.5);
				nTests++;

				positionError = (pInterp - pTest).norm();
			}
		}
		else
		{
			// Error is less than the tolerance; increase the step size until the
			// tolerance is just exceeded.
			while (positionError < tolerance && dt < maxStepSize)
			{
				dt *= stepFactor;

				p1 = positionAtTime(t + dt);
				v1 = velocityAtTime(t + dt);

				tmid = t + dt / 2.0;
				pTest = positionAtTime(tmid);
				pInterp = cubicInterpolate(lastP, lastV * dt,
					p1, v1 * dt,
					0.5);
				nTests++;

				positionError = (pInterp - pTest).norm();
			}
		}

		t = t + dt;
		lastP = p1;
		lastV = v1;

		proc.sample(t, lastP, lastV);
		sampCount++;
	}

	// Statistics for debugging
	// clog << "Orbit samples: " << sampCount << ", nTests: " << nTests << endl;
}

EllipticalOrbit::EllipticalOrbit(double _pericenterDistance,
	double _eccentricity,
	double _inclination,
	double _ascendingNode,
	double _argOfPeriapsis,
	double _meanAnomalyAtEpoch,
	double _period,
	double _epoch) :
	pericenterDistance(_pericenterDistance),
	eccentricity(_eccentricity),
	inclination(_inclination),
	ascendingNode(_ascendingNode),
	argOfPeriapsis(_argOfPeriapsis),
	meanAnomalyAtEpoch(_meanAnomalyAtEpoch),
	period(_period),
	epoch(_epoch)
{
	orbitPlaneRotation = (ZRotation(_ascendingNode) * XRotation(_inclination) * ZRotation(_argOfPeriapsis)).toRotationMatrix();
}

typedef pair<double, double> Solution;

struct SolveKeplerFunc1 : public unary_function<double, double>
{
	double ecc;
	double M;

	SolveKeplerFunc1(double _ecc, double _M) : ecc(_ecc), M(_M) {};

	double operator()(double x) const
	{
		return M + ecc * sin(x);
	}
};


// Faster converging iteration for Kepler's Equation; more efficient
// than above for orbits with eccentricities greater than 0.3.  This
// is from Jean Meeus's _Astronomical Algorithms_ (2nd ed), p. 199
struct SolveKeplerFunc2 : public unary_function<double, double>
{
	double ecc;
	double M;

	SolveKeplerFunc2(double _ecc, double _M) : ecc(_ecc), M(_M) {};

	double operator()(double x) const
	{
		return x + (M + ecc * sin(x) - x) / (1 - ecc * cos(x));
	}
};

struct SolveKeplerLaguerreConway : public unary_function<double, double>
{
	double ecc;
	double M;

	SolveKeplerLaguerreConway(double _ecc, double _M) : ecc(_ecc), M(_M) {};

	double operator()(double x) const
	{
		double s = ecc * sin(x);
		double c = ecc * cos(x);
		double f = x - s - M;
		double f1 = 1 - c;
		double f2 = s;
		x += -5 * f / (f1 + sign(f1) * sqrt(abs(16 * f1 * f1 - 20 * f * f2)));

		return x;
	}
};

struct SolveKeplerLaguerreConwayHyp : public unary_function<double, double>
{
	double ecc;
	double M;

	SolveKeplerLaguerreConwayHyp(double _ecc, double _M) : ecc(_ecc), M(_M) {};

	double operator()(double x) const
	{
		double s = ecc * sinh(x);
		double c = ecc * cosh(x);
		double f = s - x - M;
		double f1 = c - 1;
		double f2 = s;
		x += -5 * f / (f1 + sign(f1) * sqrt(abs(16 * f1 * f1 - 20 * f * f2)));

		return x;
	}
};

Vector3d Orbit::velocityAtTime(double tdb) const
{
	Vector3d p0 = positionAtTime(tdb);
	Vector3d p1 = positionAtTime(tdb + ORBITAL_VELOCITY_DIFF_DELTA);
	return (p1 - p0) * (1.0 / ORBITAL_VELOCITY_DIFF_DELTA);
}


double EllipticalOrbit::eccentricAnomaly(double M) const
{
	if (eccentricity == 0.0)
	{
		// Circular orbit
		return M;
	}
	else if (eccentricity < 0.2)
	{
		// Low eccentricity, so use the standard iteration technique
		Solution sol = solve_iteration_fixed(SolveKeplerFunc1(eccentricity, M), M, 5);
		return sol.first;
	}
	else if (eccentricity < 0.9)
	{
		// Higher eccentricity elliptical orbit; use a more complex but
		// much faster converging iteration.
		Solution sol = solve_iteration_fixed(SolveKeplerFunc2(eccentricity, M), M, 6);
		// Debugging
		// printf("ecc: %f, error: %f mas\n",
		//        eccentricity, radToDeg(sol.second) * 3600000);
		return sol.first;
	}
	else if (eccentricity < 1.0)
	{
		// Extremely stable Laguerre-Conway method for solving Kepler's
		// equation.  Only use this for high-eccentricity orbits, as it
		// requires more calcuation.
		double E = M + 0.85 * eccentricity * sign(sin(M));
		Solution sol = solve_iteration_fixed(SolveKeplerLaguerreConway(eccentricity, M), E, 8);
		return sol.first;
	}
	else if (eccentricity == 1.0)
	{
		// Nearly parabolic orbit; very common for comets
		// TODO: handle this
		return M;
	}
	else
	{
		// Laguerre-Conway method for hyperbolic (ecc > 1) orbits.
		double E = log(2 * M / eccentricity + 1.85);
		Solution sol = solve_iteration_fixed(SolveKeplerLaguerreConwayHyp(eccentricity, M), E, 30);
		return sol.first;
	}
}


// Compute the position at the specified eccentric
// anomaly E.
Vector3d EllipticalOrbit::positionAtE(double E) const
{
	double x, y;

	if (eccentricity < 1.0)
	{
		double a = pericenterDistance / (1.0 - eccentricity);
		x = a * (cos(E) - eccentricity);
		y = a * sqrt(1 - square(eccentricity)) * sin(E);
	}
	else if (eccentricity > 1.0)
	{
		double a = pericenterDistance / (1.0 - eccentricity);
		x = -a * (eccentricity - cosh(E));
		y = -a * sqrt(square(eccentricity) - 1) * sinh(E);
	}
	else
	{
		// TODO: Handle parabolic orbits
		x = 0.0;
		y = 0.0;
	}

	Vector3d p = orbitPlaneRotation * Vector3d(x, y, 0);

	// Convert to Celestia's internal coordinate system
	return Vector3d(p.x(), p.z(), -p.y());
}


// Compute the velocity at the specified eccentric
// anomaly E.
Vector3d EllipticalOrbit::velocityAtE(double E) const
{
	double x, y;

	if (eccentricity < 1.0)
	{
		double a = pericenterDistance / (1.0 - eccentricity);
		double b = a * sqrt(1 - square(eccentricity));
		double sinE = sin(E);
		double cosE = cos(E);

		double meanMotion = 2.0 * PI / period;
		double edot = meanMotion / (1 - eccentricity * cosE);

		x = -a * sinE * edot;
		y = b * cosE * edot;
	}
	else if (eccentricity > 1.0)
	{
		double a = pericenterDistance / (1.0 - eccentricity);
		x = -a * (eccentricity - cosh(E));
		y = -a * sqrt(square(eccentricity) - 1) * sinh(E);
	}
	else
	{
		// TODO: Handle parabolic orbits
		x = 0.0;
		y = 0.0;
	}

	Vector3d v = orbitPlaneRotation * Vector3d(x, y, 0);

	// Convert to Celestia's coordinate system
	return Vector3d(v.x(), v.z(), -v.y());
}


// Return the offset from the center
Vector3d EllipticalOrbit::positionAtTime(double t) const
{
	t = t - epoch;
	double meanMotion = 2.0 * PI / period;
	double meanAnomaly = meanAnomalyAtEpoch + t * meanMotion;
	double E = eccentricAnomaly(meanAnomaly);

	return positionAtE(E);
}


Vector3d EllipticalOrbit::velocityAtTime(double t) const
{
	t = t - epoch;
	double meanMotion = 2.0 * PI / period;
	double meanAnomaly = meanAnomalyAtEpoch + t * meanMotion;
	double E = eccentricAnomaly(meanAnomaly);

	return velocityAtE(E);
}


double EllipticalOrbit::getPeriod() const
{
	return period;
}


double EllipticalOrbit::getBoundingRadius() const
{
	// TODO: watch out for unbounded parabolic and hyperbolic orbits
	return pericenterDistance * ((1.0 + eccentricity) / (1.0 - eccentricity));
}


/****** Value method implementations *******/

Value::Value(double d)
{
	type = NumberType;
	data.d = d;
}

Value::Value(string s)
{
	type = StringType;
	data.s = new string(s);
}

Value::Value(Array_* a)
{
	type = ArrayType;
	data.a = a;
}

Value::Value(Hash* h)
{
	type = HashType;
	data.h = h;
}

Value::Value(bool b)
{
	type = BooleanType;
	data.d = b ? 1.0 : 0.0;
}

Value::~Value()
{
	if (type == StringType)
	{
		delete data.s;
	}
	else if (type == ArrayType)
	{
		if (data.a != NULL)
		{
			for (unsigned int i = 0; i < data.a->size(); i++)
				delete (*data.a)[i];
			delete data.a;
		}
	}
	else if (type == HashType)
	{
		if (data.h != NULL)
		{
#if 0
			Hash::iterator iter = data.h->begin();
			while (iter != data.h->end())
			{
				delete iter->second;
				iter++;
			}
#endif
			delete data.h;
		}
	}
}

/****** Value method implementations *******/

Value::ValueType Value::getType() const
{
	return type;
}

double Value::getNumber() const
{
	// ASSERT(type == NumberType);
	return data.d;
}

string Value::getString() const
{
	// ASSERT(type == StringType);
	return *data.s;
}

Array_* Value::getArray() const
{
	// ASSERT(type == ArrayType);
	return data.a;
}

Hash* Value::getHash() const
{
	// ASSERT(type == HashType);
	return data.h;
}

bool Value::getBoolean() const
{
	// ASSERT(type == BooleanType);
	return (data.d != 0.0);
}

/****** Parser method implementation ******/

Parser::Parser(Tokenizer* _tokenizer) :
	tokenizer(_tokenizer)
{
}


Array_* Parser::readArray()
{
	Tokenizer::TokenType tok = tokenizer->nextToken();
	if (tok != Tokenizer::TokenBeginArray)
	{
		tokenizer->pushBack();
		return NULL;
	}

	Array_* array = new Array_();

	Value* v = readValue();
	while (v != NULL)
	{
		array->insert(array->end(), v);
		v = readValue();
	}

	tok = tokenizer->nextToken();
	if (tok != Tokenizer::TokenEndArray)
	{
		tokenizer->pushBack();
		delete array;
		return NULL;
	}

	return array;
}


Hash* Parser::readHash()
{
	Tokenizer::TokenType tok = tokenizer->nextToken();
	if (tok != Tokenizer::TokenBeginGroup)
	{
		tokenizer->pushBack();
		return NULL;
	}

	Hash* hash = new Hash();

	tok = tokenizer->nextToken();
	while (tok != Tokenizer::TokenEndGroup)
	{
		if (tok != Tokenizer::TokenName)
		{
			tokenizer->pushBack();
			delete hash;
			return NULL;
		}
		string name = tokenizer->getNameValue();

#ifndef USE_POSTFIX_UNITS
		readUnits(name, hash);
#endif

		Value* value = readValue();
		if (value == NULL)
		{
			delete hash;
			return NULL;
		}

		hash->addValue(name, *value);

#ifdef USE_POSTFIX_UNITS
		readUnits(name, hash);
#endif

		tok = tokenizer->nextToken();
	}

	return hash;
}


/**
* Reads a units section into the hash.
* @param[in] propertyName Name of the current property.
* @param[in] hash Hash to add units quantities into.
* @return True if a units section was successfully read, false otherwise.
*/
bool Parser::readUnits(const string& propertyName, Hash* hash)
{
	Tokenizer::TokenType tok = tokenizer->nextToken();
	if (tok != Tokenizer::TokenBeginUnits)
	{
		tokenizer->pushBack();
		return false;
	}

	tok = tokenizer->nextToken();
	while (tok != Tokenizer::TokenEndUnits)
	{
		if (tok != Tokenizer::TokenName)
		{
			tokenizer->pushBack();
			return false;
		}

		string unit = tokenizer->getNameValue();
		Value* value = new Value(unit);

		if (astro::isLengthUnit(unit))
		{
			string keyName(propertyName + "%Length");
			hash->addValue(keyName, *value);
		}
		else if (astro::isTimeUnit(unit))
		{
			string keyName(propertyName + "%Time");
			hash->addValue(keyName, *value);
		}
		else if (astro::isAngleUnit(unit))
		{
			string keyName(propertyName + "%Angle");
			hash->addValue(keyName, *value);
		}
		else
		{
			delete value;
			return false;
		}

		tok = tokenizer->nextToken();
	}

	return true;
}


Value* Parser::readValue()
{
	Tokenizer::TokenType tok = tokenizer->nextToken();
	switch (tok)
	{
	case Tokenizer::TokenNumber:
		return new Value(tokenizer->getNumberValue());

	case Tokenizer::TokenString:
		return new Value(tokenizer->getStringValue());

	case Tokenizer::TokenName:
		if (tokenizer->getNameValue() == "false")
			return new Value(false);
		else if (tokenizer->getNameValue() == "true")
			return new Value(true);
		else
		{
			tokenizer->pushBack();
			return NULL;
		}

	case Tokenizer::TokenBeginArray:
		tokenizer->pushBack();
		{
			Array_* array = readArray();
			if (array == NULL)
				return NULL;
			else
				return new Value(array);
		}

	case Tokenizer::TokenBeginGroup:
		tokenizer->pushBack();
		{
			Hash* hash = readHash();
			if (hash == NULL)
				return NULL;
			else
				return new Value(hash);
		}

	default:
		tokenizer->pushBack();
		return NULL;
	}
}


AssociativeArray::AssociativeArray()
{
}

AssociativeArray::~AssociativeArray()
{
#if 0
	Hash::iterator iter = data.h->begin();
	while (iter != data.h->end())
	{
		delete iter->second;
		iter++;
	}
#endif
	for (map<string, Value*>::iterator iter = assoc.begin(); iter != assoc.end(); iter++)
		delete iter->second;
}

Value* AssociativeArray::getValue(string key) const
{
	map<string, Value*>::const_iterator iter = assoc.find(key);
	if (iter == assoc.end())
		return NULL;
	else
		return iter->second;
}

void AssociativeArray::addValue(string key, Value& val)
{
	assoc.insert(map<string, Value*>::value_type(key, &val));
}

bool AssociativeArray::getNumber(const string& key, double& val) const
{
	Value* v = getValue(key);
	if (v == NULL || v->getType() != Value::NumberType)
		return false;

	val = v->getNumber();

	return true;
}

bool AssociativeArray::getNumber(const string& key, float& val) const
{
	double dval;

	if (!getNumber(key, dval))
	{
		return false;
	}
	else
	{
		val = (float)dval;
		return true;
	}
}

bool AssociativeArray::getNumber(const string& key, int& val) const
{
	double ival;

	if (!getNumber(key, ival))
	{
		return false;
	}
	else
	{
		val = (int)ival;
		return true;
	}
}

bool AssociativeArray::getNumber(const string& key, uint32& val) const
{
	double ival;

	if (!getNumber(key, ival))
	{
		return false;
	}
	else
	{
		val = (uint32)ival;
		return true;
	}
}

bool AssociativeArray::getString(const string& key, string& val) const
{
	Value* v = getValue(key);
	if (v == NULL || v->getType() != Value::StringType)
		return false;

	val = v->getString();

	return true;
}

bool AssociativeArray::getBoolean(const string& key, bool& val) const
{
	Value* v = getValue(key);
	if (v == NULL || v->getType() != Value::BooleanType)
		return false;

	val = v->getBoolean();

	return true;
}

bool AssociativeArray::getVector(const string& key, Vec3d& val) const
{
	Value* v = getValue(key);
	if (v == NULL || v->getType() != Value::ArrayType)
		return false;

	Array_* arr = v->getArray();
	if (arr->size() != 3)
		return false;

	Value* x = (*arr)[0];
	Value* y = (*arr)[1];
	Value* z = (*arr)[2];

	if (x->getType() != Value::NumberType ||
		y->getType() != Value::NumberType ||
		z->getType() != Value::NumberType)
		return false;

	val = Vec3d(x->getNumber(), y->getNumber(), z->getNumber());

	return true;
}

bool AssociativeArray::getVector(const string& key, Vector3d& val) const
{
	Value* v = getValue(key);
	if (v == NULL || v->getType() != Value::ArrayType)
		return false;

	Array_* arr = v->getArray();
	if (arr->size() != 3)
		return false;

	Value* x = (*arr)[0];
	Value* y = (*arr)[1];
	Value* z = (*arr)[2];

	if (x->getType() != Value::NumberType ||
		y->getType() != Value::NumberType ||
		z->getType() != Value::NumberType)
		return false;

	val = Vector3d(x->getNumber(), y->getNumber(), z->getNumber());

	return true;
}

bool AssociativeArray::getVector(const string& key, Vec3f& val) const
{
	Vec3d vecVal;

	if (!getVector(key, vecVal))
		return false;

	val = Vec3f((float)vecVal.x, (float)vecVal.y, (float)vecVal.z);

	return true;
}


bool AssociativeArray::getVector(const string& key, Vector3f& val) const
{
	Vector3d vecVal;

	if (!getVector(key, vecVal))
		return false;

	val = vecVal.cast<float>();

	return true;
}


/** @copydoc AssociativeArray::getRotation() */
bool AssociativeArray::getRotation(const string& key, Quatf& val) const
{
	Value* v = getValue(key);
	if (v == NULL || v->getType() != Value::ArrayType)
		return false;

	Array_* arr = v->getArray();
	if (arr->size() != 4)
		return false;

	Value* w = (*arr)[0];
	Value* x = (*arr)[1];
	Value* y = (*arr)[2];
	Value* z = (*arr)[3];

	if (w->getType() != Value::NumberType ||
		x->getType() != Value::NumberType ||
		y->getType() != Value::NumberType ||
		z->getType() != Value::NumberType)
		return false;

	Vec3f axis((float)x->getNumber(),
		(float)y->getNumber(),
		(float)z->getNumber());
	axis.normalize();

	double ang = w->getNumber();
	double angScale = 1.0;
	getAngleScale(key, angScale);
	float angle = degToRad((float)(ang * angScale));

	val.setAxisAngle(axis, angle);

	return true;
}


/**
* Retrieves a quaternion, scaled to an associated angle unit.
*
* The quaternion is specified in the catalog file in axis-angle format as follows:
* \verbatim {PropertyName} [ angle axisX axisY axisZ ] \endverbatim
*
* @param[in] key Hash key for the rotation.
* @param[out] val A quaternion representing the value if present, unaffected if not.
* @return True if the key exists in the hash, false otherwise.
*/
bool AssociativeArray::getRotation(const string& key, Eigen::Quaternionf& val) const
{
	Value* v = getValue(key);
	if (v == NULL || v->getType() != Value::ArrayType)
		return false;

	Array_* arr = v->getArray();
	if (arr->size() != 4)
		return false;

	Value* w = (*arr)[0];
	Value* x = (*arr)[1];
	Value* y = (*arr)[2];
	Value* z = (*arr)[3];

	if (w->getType() != Value::NumberType ||
		x->getType() != Value::NumberType ||
		y->getType() != Value::NumberType ||
		z->getType() != Value::NumberType)
		return false;

	Vector3f axis((float)x->getNumber(),
		(float)y->getNumber(),
		(float)z->getNumber());

	double ang = w->getNumber();
	double angScale = 1.0;
	getAngleScale(key, angScale);
	float angle = degToRad((float)(ang * angScale));

	val = Quaternionf(AngleAxisf(angle, axis.normalized()));

	return true;
}


/**
* Retrieves a numeric quantity scaled to an associated angle unit.
* @param[in] key Hash key for the quantity.
* @param[out] val The returned quantity if present, unaffected if not.
* @param[in] outputScale Returned value is scaled to this value.
* @param[in] defaultScale If no units are specified, use this scale. Defaults to outputScale.
* @return True if the key exists in the hash, false otherwise.
*/
bool
AssociativeArray::getAngle(const string& key, double& val, double outputScale, double defaultScale) const
{
	if (!getNumber(key, val))
		return false;

	double angleScale;
	if (getAngleScale(key, angleScale))
	{
		angleScale /= outputScale;
	}
	else
	{
		angleScale = (defaultScale == 0.0) ? 1.0 : defaultScale / outputScale;
	}

	val *= angleScale;

	return true;
}


/** @copydoc AssociativeArray::getAngle() */
bool
AssociativeArray::getAngle(const string& key, float& val, double outputScale, double defaultScale) const
{
	double dval;

	if (!getAngle(key, dval, outputScale, defaultScale))
		return false;

	val = ((float)dval);

	return true;
}


/**
* Retrieves a numeric quantity scaled to an associated length unit.
* @param[in] key Hash key for the quantity.
* @param[out] val The returned quantity if present, unaffected if not.
* @param[in] outputScale Returned value is scaled to this value.
* @param[in] defaultScale If no units are specified, use this scale. Defaults to outputScale.
* @return True if the key exists in the hash, false otherwise.
*/
bool
AssociativeArray::getLength(const string& key, double& val, double outputScale, double defaultScale) const
{
	if (!getNumber(key, val))
		return false;

	double lengthScale;
	if (getLengthScale(key, lengthScale))
	{
		lengthScale /= outputScale;
	}
	else
	{
		lengthScale = (defaultScale == 0.0) ? 1.0 : defaultScale / outputScale;
	}

	val *= lengthScale;

	return true;
}


/** @copydoc AssociativeArray::getLength() */
bool AssociativeArray::getLength(const string& key, float& val, double outputScale, double defaultScale) const
{
	double dval;

	if (!getLength(key, dval, outputScale, defaultScale))
		return false;

	val = ((float)dval);

	return true;
}


/**
* Retrieves a numeric quantity scaled to an associated time unit.
* @param[in] key Hash key for the quantity.
* @param[out] val The returned quantity if present, unaffected if not.
* @param[in] outputScale Returned value is scaled to this value.
* @param[in] defaultScale If no units are specified, use this scale. Defaults to outputScale.
* @return True if the key exists in the hash, false otherwise.
*/
bool AssociativeArray::getTime(const string& key, double& val, double outputScale, double defaultScale) const
{
	if (!getNumber(key, val))
		return false;

	double timeScale;
	if (getTimeScale(key, timeScale))
	{
		timeScale /= outputScale;
	}
	else
	{
		timeScale = (defaultScale == 0.0) ? 1.0 : defaultScale / outputScale;
	}

	val *= timeScale;

	return true;
}


/** @copydoc AssociativeArray::getTime() */
bool AssociativeArray::getTime(const string& key, float& val, double outputScale, double defaultScale) const
{
	double dval;

	if (!getLength(key, dval, outputScale, defaultScale))
		return false;

	val = ((float)dval);

	return true;
}


/**
* Retrieves a vector quantity scaled to an associated length unit.
* @param[in] key Hash key for the quantity.
* @param[out] val The returned vector if present, unaffected if not.
* @param[in] outputScale Returned value is scaled to this value.
* @param[in] defaultScale If no units are specified, use this scale. Defaults to outputScale.
* @return True if the key exists in the hash, false otherwise.
*/
bool AssociativeArray::getLengthVector(const string& key, Eigen::Vector3d& val, double outputScale, double defaultScale) const
{
	if (!getVector(key, val))
		return false;

	double lengthScale;
	if (getLengthScale(key, lengthScale))
	{
		lengthScale /= outputScale;
	}
	else
	{
		lengthScale = (defaultScale == 0.0) ? 1.0 : defaultScale / outputScale;
	}

	val *= lengthScale;

	return true;
}


/** @copydoc AssociativeArray::getLengthVector() */
bool AssociativeArray::getLengthVector(const string& key, Eigen::Vector3f& val, double outputScale, double defaultScale) const
{
	Vector3d vecVal;

	if (!getLengthVector(key, vecVal, outputScale, defaultScale))
		return false;

	val = vecVal.cast<float>();

	return true;
}


/**
* Retrieves a spherical tuple \verbatim [longitude, latitude, altitude] \endverbatim scaled to associated angle and length units.
* @param[in] key Hash key for the quantity.
* @param[out] val The returned tuple in units of degrees and kilometers if present, unaffected if not.
* @return True if the key exists in the hash, false otherwise.
*/
bool AssociativeArray::getSphericalTuple(const string& key, Vector3d& val) const
{
	if (!getVector(key, val))
		return false;

	double angleScale;
	if (getAngleScale(key, angleScale))
	{
		val[0] *= angleScale;
		val[1] *= angleScale;
	}

	double lengthScale = 1.0;
	getLengthScale(key, lengthScale);
	val[2] *= lengthScale;

	return true;
}


/** @copydoc AssociativeArray::getSphericalTuple */
bool AssociativeArray::getSphericalTuple(const string& key, Vector3f& val) const
{
	Vector3d vecVal;

	if (!getSphericalTuple(key, vecVal))
		return false;

	val = vecVal.cast<float>();

	return true;
}


/**
* Retrieves the angle unit associated with a given property.
* @param[in] key Hash key for the property.
* @param[out] scale The returned angle unit scaled to degrees if present, unaffected if not.
* @return True if an angle unit has been specified for the property, false otherwise.
*/
bool AssociativeArray::getAngleScale(const string& key, double& scale) const
{
	string unitKey(key + "%Angle");
	string unit;

	if (!getString(unitKey, unit))
		return false;

	return astro::getAngleScale(unit, scale);
}


/** @copydoc AssociativeArray::getAngleScale() */
bool AssociativeArray::getAngleScale(const string& key, float& scale) const
{
	double dscale;
	if (!getAngleScale(key, dscale))
		return false;

	scale = ((float)dscale);
	return true;
}


/**
* Retrieves the length unit associated with a given property.
* @param[in] key Hash key for the property.
* @param[out] scale The returned length unit scaled to kilometers if present, unaffected if not.
* @return True if a length unit has been specified for the property, false otherwise.
*/
bool AssociativeArray::getLengthScale(const string& key, double& scale) const
{
	string unitKey(key + "%Length");
	string unit;

	if (!getString(unitKey, unit))
		return false;

	return astro::getLengthScale(unit, scale);
}


/** @copydoc AssociativeArray::getLengthScale() */
bool AssociativeArray::getLengthScale(const string& key, float& scale) const
{
	double dscale;
	if (!getLengthScale(key, dscale))
		return false;

	scale = ((float)dscale);
	return true;
}


/**
* Retrieves the time unit associated with a given property.
* @param[in] key Hash key for the property.
* @param[out] scale The returned time unit scaled to days if present, unaffected if not.
* @return True if a time unit has been specified for the property, false otherwise.
*/
bool AssociativeArray::getTimeScale(const string& key, double& scale) const
{
	string unitKey(key + "%Time");
	string unit;

	if (!getString(unitKey, unit))
		return false;

	return astro::getTimeScale(unit, scale);
}


/** @copydoc AssociativeArray::getTimeScale() */
bool AssociativeArray::getTimeScale(const string& key, float& scale) const
{
	double dscale;
	if (!getTimeScale(key, dscale))
		return false;

	scale = ((float)dscale);
	return true;
}


HashIterator
AssociativeArray::begin()
{
	return assoc.begin();
}


HashIterator
AssociativeArray::end()
{
	return assoc.end();
}
