// Fill out your copyright notice in the Description page of Project Settings.

#include "GravityComponent.h"
#include "Engine.h"
#include "orbitCreator.h"
#include "astro.h"
#include "orbit.h"
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <string>
#include "DrawDebugHelpers.h"
#include "GravityBody.h"
#include "Components/ActorComponent.h"

using namespace Eigen;

static double yearToJD(int year)
{
	return (double)astro::Date(year, 1, 1);
}
// Sets default values for this component's properties
UGravityComponent::UGravityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	time = yearToJD(2000);
	
	// ...
}

void UGravityComponent::PostLoad()
{

	Super::PostLoad();
	
}
// Called when the game starts
void UGravityComponent::BeginPlay()
{

	Super::BeginPlay();
	ProcessOrbits(10);
	// ...
	
}


// Called every frame
void UGravityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	Move(DeltaTime);
	// ...
}

FVector UGravityComponent::RecalculateCoordinates(FVector input, FVector location)
{
	FVector result = location + input;
	return result;
}

FVector UGravityComponent::RotateCoordinates(FVector input, FRotator rotation)
{
	input = input.RotateAngleAxis(rotation.Roll, FVector::ForwardVector);
	input = input.RotateAngleAxis(rotation.Pitch, FVector::RightVector);
	input = input.RotateAngleAxis(rotation.Yaw, FVector::UpVector);
	return input;
}

void UGravityComponent::ProcessOrbits(float scale)
{
	OrbitCreator* creator = new OrbitCreator();

	orbitDic.Empty();

	FVector location = GetOwner()->GetTransform().GetLocation();

	double t0 = yearToJD(2000);

	TArray<AActor*> children;
	GetOwner()->GetAttachedActors(children);
	for (int i = 0; i < children.Num(); i++) {
		AActor* body = children[i];
		UGravityBody* Component = nullptr;
		Component = body->FindComponentByClass<UGravityBody>();
		if (Component != nullptr)
		{
			FVector bodyLocation = RecalculateCoordinates(body->GetTransform().GetLocation(), location)*Component->orbitScale;
			FVector bodyVelocity = Component->velocity;
			Vector3d v_bodyLocation = Vector3d(bodyLocation.X, bodyLocation.Y, bodyLocation.Z);
			Vector3d v_bodyVelocity = Vector3d(bodyVelocity.X, bodyVelocity.Y, bodyVelocity.Z);
			double mass = astro::EarthMass;

			EllipticalOrbit* eo = creator->StateVectorToOrbit(v_bodyLocation, v_bodyVelocity, mass, t0);
			eo->rotation = Component->rotation;
			if (!orbitDic.Contains(Component))
			{
				orbitDic.Add(Component, eo);
			}
		}
	}
	DrawOrbits();
}

void UGravityComponent::DrawOrbits()
{
	//UKismetSystemLibrary::FlushPersistentDebugLines(GetWorld());
	FVector location = GetOwner()->GetTransform().GetLocation();

	for (auto& orbit : orbitDic)
	{
		UGravityBody* body = orbit.Key;
		if (body->drawOrbit)
		{
			double t0 = yearToJD(2000);
			double period = orbit.Value->getPeriod();
			double step = period* 1.0e-3;
			double t1 = t0 + period;
			double t = t0;
			while (t < t1)
			{

				Vector3d p0 = orbit.Value->positionAtTime(t);
				Vector3d p1 = orbit.Value->positionAtTime(t + step);

				FVector start;
				start.X = p0.x() / body->orbitScale;
				start.Y = p0.y() / body->orbitScale;
				start.Z = p0.z() / body->orbitScale;


				FVector end;
				end.X = p1.x() / body->orbitScale;
				end.Y = p1.y() / body->orbitScale;
				end.Z = p1.z() / body->orbitScale;

				DrawDebugLine(
					GetWorld(),
					RecalculateCoordinates(RotateCoordinates(start, orbit.Value->rotation), location),
					RecalculateCoordinates(RotateCoordinates(end, orbit.Value->rotation), location),
					body->color,
					true, -1, 0,
					3
				);

				t += step;
			}
		}
	}
}


void UGravityComponent::Move(float delateTime)
{

	FVector location = GetOwner()->GetTransform().GetLocation();
	double t0 = yearToJD(2000);
	
	auto diff = time - t0;
	for (auto& orbit : orbitDic)
	{
		double period = orbit.Value->getPeriod();

		UGravityBody* body = orbit.Key;
		AActor* owner = body->GetOwner();

		double step = period* 1.0e-3;
		time += step;
		
		Vector3d position = orbit.Value->positionAtTime(t0 + diff/body->timeScale)/body->orbitScale;
		Vector3d velocity = orbit.Value->velocityAtTime(t0 + diff / body->timeScale) / (24 * 60 * 60);
		FVector f_position;
		f_position.X = position.x();
		f_position.Y = position.y();
		f_position.Z = position.z();

		body->velocity.X = velocity.x();
		body->velocity.Y = velocity.y();
		body->velocity.Z = velocity.z();

		owner->SetActorLocation(RecalculateCoordinates(RotateCoordinates(f_position, orbit.Value->rotation), location));
	}
}




