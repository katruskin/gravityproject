#include "orbitCreator.h"
#include "stdafx.h"
#include "astro.h"
#include "orbit.h"
#include <geomutil.h>
#include <cassert>
#include <Eigen/Core>
#include <Eigen/Geometry>

using namespace Eigen;
/**
* Returns the default units scale for orbits.
*
* If the usePlanetUnits flag is set, this returns a distance scale of AU and a
* time scale of years. Otherwise the distace scale is kilometers and the time
* scale is days.
*
* @param[in] usePlanetUnits Controls whether to return planet units or satellite units.
* @param[out] distanceScale The default distance scale in kilometers.
* @param[out] timeScale The default time scale in days.
*/


struct OrbitalElements
{
	double semimajorAxis;
	double eccentricity;
	double inclination;
	double longAscendingNode;
	double argPericenter;
	double meanAnomaly;
	double period;
};

static void StateVectorToElements(const Vector3d& position,
	const Vector3d& v,
	double GM,
	OrbitalElements* elements)
{
	Vector3d R = position;
	Vector3d L = R.cross(v);
	double magR = R.norm();
	double magL = L.norm();
	double magV = v.norm();
	L *= (1.0 / magL);

	Vector3d W = L.cross(R / magR);

	// Compute the semimajor axis
	double a = 1.0 / (2.0 / magR - square(magV) / GM);

	// Compute the eccentricity
	double p = square(magL) / GM;
	double q = R.dot(v);
	double ex = 1.0 - magR / a;
	double ey = q / sqrt(a * GM);
	double e = sqrt(ex * ex + ey * ey);

	// Compute the mean anomaly
	double E = atan2(ey, ex);
	double M = E - e * sin(E);

	// Compute the inclination
	double cosi = L.dot(Vector3d::UnitY());
	double i = 0.0;
	if (cosi < 1.0)
		i = acos(cosi);

	// Compute the longitude of ascending node
	double Om = atan2(L.x(), L.z());

	// Compute the argument of pericenter
	Vector3d U = R / magR;
	double s_nu = v.dot(U) * sqrt(p / GM);
	double c_nu = v.dot(W) * sqrt(p / GM) - 1;
	s_nu /= e;
	c_nu /= e;
	Vector3d P = U * c_nu - W * s_nu;
	Vector3d Q = U * s_nu + W * c_nu;
	double om = atan2(P.y(), Q.y());

	// Compute the period
	double T = 2 * PI * sqrt(cube(a) / GM);

	elements->semimajorAxis = a;
	elements->eccentricity = e;
	elements->inclination = i;
	elements->longAscendingNode = Om;
	elements->argPericenter = om;
	elements->meanAnomaly = M;
	elements->period = T;
}

static void
GetDefaultUnits(bool usePlanetUnits, double& distanceScale, double& timeScale)
{
	if (usePlanetUnits)
	{
		distanceScale = KM_PER_AU;
		timeScale = DAYS_PER_YEAR;
	}
	else
	{
		distanceScale = 1.0;
		timeScale = 1.0;
	}
}

bool
ParseDate(Hash* hash, const string& name, double& jd)
{
	// Check first for a number value representing a Julian date
	if (hash->getNumber(name, jd))
		return true;

	string dateString;
	if (hash->getString(name, dateString))
	{
		astro::Date date(1, 1, 1);
		if (astro::parseDate(dateString, date))
		{
			jd = (double)date;
			return true;
		}
	}

	return false;
}


OrbitCreator::OrbitCreator()
{
}

OrbitCreator::~OrbitCreator()
{
}

EllipticalOrbit*
OrbitCreator::CreateEllipticalOrbit(double semiMajorAxis, double period, double inclination, double eccentricity, double ascendingNode, double argOfPericenter, double anomalyAtEpoch, double epoch)
{

	// default units for planets are AU and years, otherwise km and days

	double distanceScale;
	double timeScale;
	GetDefaultUnits(false, distanceScale, timeScale);

	double pericenterDistance;
	// If we read the semi-major axis, use it to compute the pericenter
	// distance.
	if (semiMajorAxis != 0.0)
		pericenterDistance = semiMajorAxis * (1.0 - eccentricity);

	return new EllipticalOrbit(pericenterDistance,
		eccentricity,
		degToRad(inclination),
		degToRad(ascendingNode),
		degToRad(argOfPericenter),
		degToRad(anomalyAtEpoch),
		period,
		epoch);
}

EllipticalOrbit*
OrbitCreator::StateVectorToOrbit(Vector3d position, Vector3d v, double mass, double t)
{
	Vector3d R = position;
	Vector3d L = R.cross(v);
	double magR = R.norm();
	double magL = L.norm();
	double magV = v.norm();
	L *= (1.0 / magL);

	Vector3d W = L.cross(R / magR);

	double G = astro::G * 1e-9; // convert from meters to kilometers
	double GM = G * mass;

	// Compute the semimajor axis
	double a = 1.0 / (2.0 / magR - square(magV) / GM);

	// Compute the eccentricity
	double p = square(magL) / GM;
	double q = R.dot(v);
	double ex = 1.0 - magR / a;
	double ey = q / sqrt(a * GM);
	double e = sqrt(ex * ex + ey * ey);

	// Compute the mean anomaly
	double E = atan2(ey, ex);
	double M = E - e * sin(E);

	// Compute the inclination
	double cosi = L.dot(Vector3d::UnitY());
	double i = 0.0;
	if (cosi < 1.0)
		i = acos(cosi);

	// Compute the longitude of ascending node
	double Om = atan2(L.x(), L.z());

	// Compute the argument of pericenter
	Vector3d U = R / magR;
	double s_nu = (v.dot(U)) * sqrt(p / GM);
	double c_nu = (v.dot(W)) * sqrt(p / GM) - 1;
	s_nu /= e;
	c_nu /= e;
	Vector3d P = U * c_nu - W * s_nu;
	Vector3d Q = U * s_nu + W * c_nu;
	double om = atan2(P.y(), Q.y());

	// Compute the period
	double T = 2 * PI * sqrt(cube(a) / GM);
	T = T / 86400.0; // Convert from seconds to days

	return new EllipticalOrbit(a * (1 - e), e, i, Om, om, M, T, t);
}