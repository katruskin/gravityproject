
#ifndef _CELENGINE_ORBIT_H_
#define _CELENGINE_ORBIT_H_

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <basictypes.h>
#include "tokenizer.h"
#include <vecmath.h>
#include <quaternion.h>
#include <vector>
#include <map>

class Value;

typedef map<std::string, Value*>::const_iterator HashIterator;

class AssociativeArray
{
public:
	AssociativeArray();
	~AssociativeArray();

	Value* getValue(string) const;
	void addValue(string, Value&);

	bool getNumber(const std::string&, double&) const;
	bool getNumber(const std::string&, float&) const;
	bool getNumber(const std::string&, int&) const;
	bool getNumber(const std::string&, uint32&) const;
	bool getString(const std::string&, std::string&) const;
	bool getBoolean(const std::string&, bool&) const;
	bool getVector(const std::string&, Eigen::Vector3d&) const;
	bool getVector(const std::string&, Eigen::Vector3f&) const;
	bool getVector(const std::string&, Vec3d&) const;
	bool getVector(const std::string&, Vec3f&) const;
	bool getRotation(const std::string&, Quatf&) const;
	bool getRotation(const std::string&, Eigen::Quaternionf&) const;
	bool getAngle(const std::string&, double&, double = 1.0, double = 0.0) const;
	bool getAngle(const std::string&, float&, double = 1.0, double = 0.0) const;
	bool getLength(const std::string&, double&, double = 1.0, double = 0.0) const;
	bool getLength(const std::string&, float&, double = 1.0, double = 0.0) const;
	bool getTime(const std::string&, double&, double = 1.0, double = 0.0) const;
	bool getTime(const std::string&, float&, double = 1.0, double = 0.0) const;
	bool getLengthVector(const std::string&, Eigen::Vector3d&, double = 1.0, double = 0.0) const;
	bool getLengthVector(const std::string&, Eigen::Vector3f&, double = 1.0, double = 0.0) const;
	bool getSphericalTuple(const std::string&, Eigen::Vector3d&) const;
	bool getSphericalTuple(const std::string&, Eigen::Vector3f&) const;
	bool getAngleScale(const std::string&, double&) const;
	bool getAngleScale(const std::string&, float&) const;
	bool getLengthScale(const std::string&, double&) const;
	bool getLengthScale(const std::string&, float&) const;
	bool getTimeScale(const std::string&, double&) const;
	bool getTimeScale(const std::string&, float&) const;

	HashIterator begin();
	HashIterator end();

private:
	map<string, Value*> assoc;
};

typedef vector<Value*> Array_;
typedef AssociativeArray Hash;

class Value
{
public:
	enum ValueType {
		NumberType = 0,
		StringType = 1,
		ArrayType = 2,
		HashType = 3,
		BooleanType = 4
	};

	Value(double);
	Value(string);
	Value(Array_*);
	Value(Hash*);
	Value(bool);
	~Value();

	ValueType getType() const;

	double getNumber() const;
	string getString() const;
	Array_* getArray() const;
	Hash* getHash() const;
	bool getBoolean() const;

private:
	ValueType type;

	union {
		string* s;
		double d;
		Array_* a;
		Hash* h;
	} data;
};

class Parser
{
public:
	Parser(Tokenizer*);

	Value* readValue();

private:
	Tokenizer* tokenizer;

	bool readUnits(const std::string&, Hash*);
	Array_* readArray();
	Hash* readHash();
};
class OrbitSampleProc
{
public:
	virtual ~OrbitSampleProc() {};

	virtual void sample(double t, const Eigen::Vector3d& position, const Eigen::Vector3d& velocity) = 0;
};

class Orbit
{
public:
	//virtual ~Orbit();


	/*! Return the position in the orbit's reference frame at the specified
	* time (TDB). Units are kilometers.
	*/
	virtual Eigen::Vector3d positionAtTime(double jd) const = 0;

	/*! Return the orbital velocity in the orbit's reference frame at the
	* specified time (TDB). Units are kilometers per day. If the method
	* is not overridden, the velocity will be computed by differentiation
	* of position.
	*/
	virtual Eigen::Vector3d velocityAtTime(double) const;

	virtual double getPeriod() const = 0;
	virtual double getBoundingRadius() const = 0;

	virtual void sample(double startTime, double endTime, OrbitSampleProc& proc) const;

	virtual bool isPeriodic() const { return true; };

	// Return the time range over which the orbit is valid; if the orbit
	// is always valid, begin and end should be equal.
	virtual void getValidRange(double& begin, double& end) const
	{
		begin = 0.0; end = 0.0;
	};

	struct AdaptiveSamplingParameters
	{
		double tolerance;
		double startStep;
		double minStep;
		double maxStep;
	};

	void adaptiveSample(double startTime, double endTime, OrbitSampleProc& proc, const AdaptiveSamplingParameters& samplingParameters) const;

};


class EllipticalOrbit : public Orbit
{
public:
	EllipticalOrbit(double, double, double, double, double, double, double,
		double _epoch = 2451545.0);
	//virtual ~EllipticalOrbit() {};

	// Compute the orbit for a specified Julian date
	virtual Eigen::Vector3d positionAtTime(double) const;
	virtual Eigen::Vector3d velocityAtTime(double) const;
	double getPeriod() const;
	double getBoundingRadius() const;

	double pericenterDistance;
	double eccentricity;
	double inclination;
	double ascendingNode;
	double argOfPeriapsis;
	double meanAnomalyAtEpoch;
	double period;
	double epoch;
	FRotator rotation;
	Eigen::Matrix3d orbitPlaneRotation;

private:
	double eccentricAnomaly(double) const;
	Eigen::Vector3d positionAtE(double) const;
	Eigen::Vector3d velocityAtE(double) const;


};


#endif // _CELENGINE_ORBIT_H_